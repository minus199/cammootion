package com.minus.cammootion;

import android.app.Application;

import com.urbanairship.UAirship;

public class App extends Application {

	@Override
	public void onCreate() {
		UAirship.takeOff(this, new UAirship.OnReadyCallback() {
			@Override
			public void onAirshipReady(UAirship airship) {
				airship.getPushManager().setUserNotificationsEnabled(true);
				UAirship.shared().getPushManager().setUserNotificationsEnabled(true);
				CammootionNotificationFactory notificationFactory
						= new CammootionNotificationFactory(UAirship.getApplicationContext());
				airship.getPushManager().setNotificationFactory(notificationFactory);
			}
		});
	}

}
