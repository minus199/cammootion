package com.minus.cammootion;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by minus on 10/4/16.
 */

public class MotionEvent {
	@JsonProperty(value = "Messaged")
	private String Messaged;
	@JsonProperty(value = "Emailed")
	private String Emailed;
	@JsonProperty(value = "Archived")
	private String Archived;
	@JsonProperty(value = "Height")
	private String Height;
	@JsonProperty(value = "Frames")
	private Integer Frames;
	@JsonProperty(value = "Videoed")
	private String Videoed;
	@JsonProperty(value = "AlarmFrames")
	private Integer AlarmFrames;
	@JsonProperty(value = "Notes")
	private String Notes;
	@JsonProperty(value = "Uploaded")
	private String Uploaded;
	@JsonProperty(value = "Name")
	private String Name;
	@JsonProperty(value = "TotScore")
	private String TotScore;
	@JsonProperty(value = "MaxScore")
	private String MaxScore;
	@JsonProperty(value = "MonitorId")
	private String MonitorId;
	@JsonProperty(value = "AvgScore")
	private String AvgScore;
	@JsonProperty(value = "Length")
	private String Length;
	@JsonProperty(value = "Id")
	private String Id;
	@JsonProperty(value = "EndTime")
	private String EndTime;
	@JsonProperty(value = "Executed")
	private String Executed;
	@JsonProperty(value = "StartTime")
	private String StartTime;
	@JsonProperty(value = "Width")
	private String Width;
	@JsonProperty(value = "Cause")
	private String Cause;

	public MotionEvent() {
	}

	public String getMessaged() {
		return Messaged;
	}

	public String getEmailed() {
		return Emailed;
	}

	public String getArchived() {
		return Archived;
	}

	public String getHeight() {
		return Height;
	}

	public Integer getFrames() {
		return Frames;
	}

	public String getVideoed() {
		return Videoed;
	}

	public Integer getAlarmFrames() {
		return AlarmFrames;
	}

	public String getNotes() {
		return Notes;
	}

	public String getUploaded() {
		return Uploaded;
	}

	public String getName() {
		return Name;
	}

	public String getTotScore() {
		return TotScore;
	}

	public String getMaxScore() {
		return MaxScore;
	}

	public String getMonitorId() {
		return MonitorId;
	}

	public String getAvgScore() {
		return AvgScore;
	}

	public String getLength() {
		return Length;
	}

	public String getId() {
		return Id;
	}

	public String getEndTime() {
		return EndTime;
	}

	public String getExecuted() {
		return Executed;
	}

	public String getStartTime() {
		return StartTime;
	}

	public String getWidth() {
		return Width;
	}

	public String getCause() {
		return Cause;
	}

	@Override
	public String toString() {
		return "MotionEvent{" +
				"alarmFrames=" + getAlarmFrames() +
				", archived='" + getArchived() + '\'' +
				", avgScore='" + getAvgScore() + '\'' +
				", cause='" + getCause() + '\'' +
				", emailed='" + getEmailed() + '\'' +
				", endTime='" + getEndTime() + '\'' +
				", executed='" + getExecuted() + '\'' +
				", frames=" + getFrames() +
				", height='" + getHeight() + '\'' +
				", id='" + getId() + '\'' +
				", length='" + getLength() + '\'' +
				", maxScore='" + getMaxScore() + '\'' +
				", messaged='" + getMessaged() + '\'' +
				", monitorId='" + getMonitorId() + '\'' +
				", name='" + getName() + '\'' +
				", notes='" + getNotes() + '\'' +
				", startTime='" + getStartTime() + '\'' +
				", totScore='" + getTotScore() + '\'' +
				", uploaded='" + getUploaded() + '\'' +
				", videoed='" + getVideoed() + '\'' +
				", width='" + getWidth() + '\'' +
				'}';
	}
}
