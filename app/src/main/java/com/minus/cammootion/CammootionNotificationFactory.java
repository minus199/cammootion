package com.minus.cammootion;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.RuntimeJsonMappingException;
import com.urbanairship.push.PushMessage;
import com.urbanairship.push.notifications.DefaultNotificationFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by minus on 10/4/16.
 */

public class CammootionNotificationFactory extends DefaultNotificationFactory {

	public CammootionNotificationFactory(@NonNull Context context) {
		super(context);
	}

	@Override
	public NotificationCompat.Builder extendBuilder
			(@NonNull NotificationCompat.Builder builder, @NonNull PushMessage message, int notificationId) {

		final Resources res = getContext().getResources();

		final Bitmap picture = BitmapFactory.decodeResource(res, R.drawable.ic_stat_cammootion);

		final Set<String> keys = message.getPushBundle().keySet();
		final HashMap<String, String> payload = new HashMap<>();
		for (String key : keys) {
			payload.put(key, String.valueOf(message.getPushBundle().get(key)));
		}
		final String eventJson = String.valueOf(message.getPushBundle().get("com.urbanairship.push.ALERT"));
		final PushEvent pushEvent;
		try {
			pushEvent = new ObjectMapper().readValue(eventJson, PushEvent.class);
		} catch (IOException e) {
			throw new RuntimeJsonMappingException("JSON ERROR TODOTDOTDO pink patnher");
		}

		final String title = res.getString(R.string.cammootion_notification_title_template);

		builder
				.setDefaults(Notification.DEFAULT_ALL)
				.setSmallIcon(R.drawable.ic_stat_cammootion)
				.setContentTitle(title)
				.setContentText("Event status indicator")

				.setPriority(NotificationCompat.PRIORITY_DEFAULT)
				.setLargeIcon(picture)
				.setTicker("Cammooootion")
				.setNumber(420)
				.setWhen(pushEvent.getStartTimestamp().getTime())

				// Set the pending intent to be initiated when the user touches the notification.
				.setContentIntent(
						PendingIntent.getActivity(
								getContext(),
								0,
								new Intent(Intent.ACTION_VIEW, Uri.parse(pushEvent.getUrl())),
								PendingIntent.FLAG_UPDATE_CURRENT))

				// expanded list of items
				.setStyle(new NotificationCompat.InboxStyle()
						.addLine(pushEvent.getStartTime())
						.addLine(pushEvent.getEndTime())
						.addLine(pushEvent.getAvgScore())
						.addLine(pushEvent.getMaxScore())
						.addLine(pushEvent.getTotalScore())
						.addLine(pushEvent.getAlarmFrames())
						.addLine(pushEvent.getFrames())
						.setBigContentTitle(title + " detected!")
						.setSummaryText("Event appears to be in progress"))

				.addAction(
						R.drawable.ic_action_stat_share,
						res.getString(R.string.action_share),
						PendingIntent.getActivity(
								getContext(),
								0,
								Intent.createChooser(new Intent(Intent.ACTION_SEND)
										.setType("text/plain")
										.putExtra(Intent.EXTRA_TEXT, "Dummy text"), "Dummy title"),
								PendingIntent.FLAG_UPDATE_CURRENT))
				.addAction(
						R.drawable.ic_action_stat_reply,
						res.getString(R.string.action_reply),
						null)

//				.setUsesChronometer(true)
				// Automatically dismiss the notification when it is touched.
				.setAutoCancel(true);

		return builder;
	}
}
