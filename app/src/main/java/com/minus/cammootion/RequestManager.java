package com.minus.cammootion;

import android.content.Context;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by minus on 10/4/16.
 */

public class RequestManager {
	private static RequestManager mInstance;
	private static Context mCtx;
	private RequestQueue mRequestQueue;

	private RequestManager(Context context) {
		mCtx = context;
		mRequestQueue = getRequestQueue();
	}

	public static synchronized RequestManager getInstance(Context context) {
		if (mInstance == null) {
			mInstance = new RequestManager(context);
		}
		return mInstance;
	}

	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			// getApplicationContext() is key, it keeps you from leaking the
			// Activity or BroadcastReceiver if someone passes one in.
			mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
		}
		return mRequestQueue;
	}

	public <T> RequestManager addToRequestQueue(Request<T> req) {
		getRequestQueue().add(req);
		return this;
	}
}