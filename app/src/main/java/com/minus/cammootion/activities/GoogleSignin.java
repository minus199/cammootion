package com.minus.cammootion.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.minus.cammootion.R;
import com.minus.cammootion.RequestManager;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class GoogleSignin extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_google_signin);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//				.requestIdToken()
				.requestEmail()
				.build();

		final GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this)
				.enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
					@Override
					public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
						System.out.println(connectionResult);
					}
				})
				.addApi(Auth.GOOGLE_SIGN_IN_API, gso)
				.build();

		Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
		startActivityForResult(signInIntent, 200);
		/*SignInButton googleSignInButton = (SignInButton) findViewById(R.id.sign_in_button);
		googleSignInButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

			}
		});*/
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
		if (requestCode == 200) {
			GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
			final GoogleSignInAccount signInAccount = result.getSignInAccount();

			if (signInAccount != null) {
				final StringRequest request = new StringRequest(
						Request.Method.POST,
						getString(R.string.CAMERA_HOST_API) + "/user",
						new Response.Listener<String>() {
							@Override
							public void onResponse(String response) {

								System.out.println("success: " + response);
							}
						},
						new Response.ErrorListener() {
							@Override
							public void onErrorResponse(VolleyError error) {
								System.out.println("failed: " + error);
							}
						}) {
					@Override
					protected Map<String, String> getParams() {
						return signInToJson(signInAccount);
					}
				};

				RequestManager.getInstance(getApplicationContext()).addToRequestQueue(request);
			}
		}
	}

	private Map<String, String> signInToJson(GoogleSignInAccount signInAccount) {
		Map<String, String> output = new HashMap<>();
		output.put("DisplayName", signInAccount.getDisplayName());
		output.put("Email", signInAccount.getEmail());
		output.put("FamilyName", signInAccount.getFamilyName());
		output.put("GivenName", signInAccount.getGivenName());
		output.put("Id", signInAccount.getId());
//		output.put("IdToken", signInAccount.getIdToken());
		output.put("PhotoUrl", signInAccount.getPhotoUrl().toString());

		return output;
	}
}
