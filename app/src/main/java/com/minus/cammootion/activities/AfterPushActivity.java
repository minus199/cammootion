package com.minus.cammootion.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.minus.cammootion.R;
import com.urbanairship.UAirship;
import com.urbanairship.richpush.RichPushMessage;

public class AfterPushActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_push);
        UAirship.shared().getInbox().startInboxActivity();

        RichPushMessage message = null;

        /*if (RichPushInbox.VIEW_INBOX_INTENT_ACTION.equals(intent.getAction()) {
            if (getIntent().getData().getScheme().equalsIgnoreCase(RichPushInbox.MESSAGE_DATA_SCHEME)) {
                String messageId = getIntent().getData().getSchemeSpecificPart();
                message = UAirship.shared().getInbox().getMessage(messageId);
            }
        }*/
    }
}
