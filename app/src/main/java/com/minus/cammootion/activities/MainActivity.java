package com.minus.cammootion.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.RuntimeJsonMappingException;
import com.minus.cammootion.MotionEvent;
import com.minus.cammootion.R;
import com.minus.cammootion.RequestManager;

import java.io.IOException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity
		implements NavigationView.OnNavigationItemSelectedListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		final ListView eventsList = (ListView) findViewById(R.id.eventsList);

		Authenticator.setDefault(new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(getString(R.string.CAMERA_HOST_USERNAME), getString(R.string.CAMERA_HOST_PASSWORD).toCharArray());
			}
		});

		String url = getString(
				R.string.CAMERA_HOST_AUTH,
				getString(R.string.CAMERA_HOST_USERNAME),
				getString(R.string.CAMERA_HOST_PASSWORD)
		) + "/zm/api/events.json?page=1";

		StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						final Map<String, List<Map>> rawData;

						try {
							rawData = new ObjectMapper().readValue(response, Map.class);
						} catch (IOException e) {
							throw new RuntimeJsonMappingException(e.getMessage());
						}

						final List<Map> rawEvents = rawData.get("events");
						final List<MotionEvent> motionEvents = new ArrayList<>();
						final List<String> motionEventsStrings = new ArrayList<>();

						for (Map rawEvent : rawEvents) {
							final MotionEvent event = new ObjectMapper().convertValue(rawEvent.get("Event"), MotionEvent.class);
							motionEvents.add(event);
							motionEventsStrings.add("lalalal");
						}

						final ArrayAdapter<String> motionEventArrayAdapter = new ArrayAdapter<>(
								getApplicationContext(),
								android.R.layout.simple_list_item_2,
								android.R.id.text1,
								motionEventsStrings
						);

						// Assign adapter to ListView
						eventsList.setAdapter(motionEventArrayAdapter);

						System.out.println();
					}
				}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				System.out.println();
//				mTextView.setText("That didn't work!");
			}
		});

		RequestManager.getInstance(getApplicationContext()).addToRequestQueue(stringRequest).getRequestQueue().start();
		eventsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
			                        int position, long id) {

				// ListView Clicked item index
				int itemPosition = position;

				// ListView Clicked item value
				String itemValue = (String) eventsList.getItemAtPosition(position);

				// Show Alert
				Toast.makeText(getApplicationContext(),
						"Position :" + itemPosition + "  ListItem : " + itemValue, Toast.LENGTH_LONG)
						.show();
			}
		});

		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				/*Intent intent = new Intent(MainActivity.this, GoogleSignin.class);
				*//*EditText editText = (EditText) findViewById(R.id.edit_message);
				String message = editText.getText().toString();
				intent.putExtra(EXTRA_MESSAGE, message);*//*
				startActivity(intent);*/



				Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
						.setAction("Action", null).show();
			}
		});

		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
				this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.setDrawerListener(toggle);
		toggle.syncState();

		NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
		navigationView.setNavigationItemSelectedListener(this);
	}

	@Override
	public void onBackPressed() {
		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		} else {
			super.onBackPressed();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@SuppressWarnings("StatementWithEmptyBody")
	@Override
	public boolean onNavigationItemSelected(MenuItem item) {
		// Handle navigation view item clicks here.
		int id = item.getItemId();

		if (id == R.id.nav_camera) {
			// Handle the camera action
		} else if (id == R.id.nav_gallery) {

		} else if (id == R.id.nav_slideshow) {

		} else if (id == R.id.nav_manage) {

		} else if (id == R.id.nav_share) {

		} else if (id == R.id.nav_send) {

		}

		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawer.closeDrawer(GravityCompat.START);
		return true;
	}
}
