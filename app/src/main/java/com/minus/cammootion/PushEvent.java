package com.minus.cammootion;

import android.graphics.Color;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by minus on 10/4/16.
 */
class PushEvent {
	private Integer alarmFrames;
	private Integer frames;
	private Integer avgScore;
	private Integer maxScore;
	private Integer totalScore;
	private String startTime;
	private String endTime;
	private String url;

	public PushEvent() {
	}

	public static Timestamp convertStringToTimestamp(String str_date, String format) {
		try {
			DateFormat formatter = new SimpleDateFormat(format);
			return new Timestamp(formatter.parse(str_date).getTime());
		} catch (ParseException e) {
			System.out.println("Exception :" + e);
			return null;
		}
	}

	public SpannableStringBuilder getAlarmFrames() {
		return asItem("alarmFrames", alarmFrames);
	}

	public SpannableStringBuilder getFrames() {
		return asItem("frames", frames);
	}

	public SpannableStringBuilder getAvgScore() {
		return asItem("maxScore", avgScore);
	}

	public SpannableStringBuilder getMaxScore() {
		return asItem("maxScore", maxScore);
	}

	public SpannableStringBuilder getTotalScore() {
		return asItem("totalScore", totalScore);
	}

	public Timestamp getStartTimestamp() {
		return convertStringToTimestamp(startTime, "yyyy-mm-dd HH:mm:ss.S");
	}

	public SpannableStringBuilder getStartTime() {
		return asItem("startTime", startTime);
	}

	public Timestamp getEndTimestanp() {
		return convertStringToTimestamp(endTime, "yyyy-mm-dd HH:mm:ss.S");
	}

	public SpannableStringBuilder getEndTime() {
		return asItem("endTime", endTime);
	}

	public String getUrl() {
		return url;
	}

	private SpannableStringBuilder asItem(String name, Object key) {
		final SpannableStringBuilder eventItem = new SpannableStringBuilder();
		eventItem.setSpan(new ForegroundColorSpan(Color.WHITE), 0, eventItem.length(), 0);

		return eventItem.append(name).append(": ").append(String.valueOf(key));
	}

	@Override
	public String toString() {
		return "alarmFrames=" + alarmFrames +
				", frames=" + frames +
				", avgScore=" + avgScore +
				", maxScore=" + maxScore +
				", startTime='" + startTime +
				", endTime='" + endTime;
	}
}